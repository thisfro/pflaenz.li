/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */

// any CSS you import will output into a single css file (app.css in this case)
import './styles/app.scss';

// start the Stimulus application
require('bootstrap');

// Fontawesome
import '@fortawesome/fontawesome-free/js/fontawesome'
import '@fortawesome/fontawesome-free/js/solid'
import '@fortawesome/fontawesome-free/js/regular'
import '@fortawesome/fontawesome-free/js/brands'

// Cookie-consent
import 'cookie-notice/dist/cookie.notice.min';

new cookieNoticeJS({
    // Position for the cookie-notifier (default=bottom)
    'cookieNoticePosition': 'bottom',

    // The message will be shown again in X days
    'expiresIn': 365,

    // Specify a custom font family and size in pixels
    'fontFamily': 'inherit',
    'fontSize': '.9rem',

    // Dismiss button background color
    'buttonBgColor': '#343a40',

    // Dismiss button text color
    'buttonTextColor': '#fff',

    // Notice background color
    'noticeBgColor': '#000',

    // Notice text color
    'noticeTextColor': '#fff',

    // Print debug output to the console (default=false)
    'debug': false
});