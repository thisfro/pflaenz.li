// Friendly captcha
import { WidgetInstance } from 'friendly-challenge';
const $ = require('jquery');

function doneCallback(solution) {
    $('#registration_form_captcha_solution').val(solution);
}

const element = document.querySelector('#captcha');
const options = {
    doneCallback: doneCallback,
    sitekey: 'FCMVL79DP1G5K1K0',
}
const widget = new WidgetInstance(element, options);
widget.start()