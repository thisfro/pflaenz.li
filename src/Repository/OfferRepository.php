<?php

namespace App\Repository;

use App\Entity\Offer;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Offer|null find($id, $lockMode = null, $lockVersion = null)
 * @method Offer|null findOneBy(array $criteria, array $orderBy = null)
 * @method Offer[]    findAll()
 * @method Offer[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class OfferRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Offer::class);
    }

    // /**
    //  * @return Offer[] Returns an array of Offer objects
    //  */
    public function findByUser($user)
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.byUser = :val')
            ->setParameter('val', $user)
            ->orderBy('o.id', 'ASC')
            ->getQuery()
            ->getResult()
        ;
    }

    public function findById($id)
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.Id = :val')
            ->setParameter('val', $id)
            ->orderBy('o.id', 'ASC')
            ->getQuery()
            ->getResult()
        ;
    }

    public function findBySearchLiteral(string $literal)
    {
        $qb = $this->createQueryBuilder('o');
        $qb->andWhere($qb->expr()->like('o.title', ':lit'))
            ->setParameter('lit', '%' . $literal . '%')
            ->orderBy('o.id', 'ASC')
        ;

        $qb = $qb->getQuery()->getResult();

        return $qb;
    }

    /*
    public function findOneBySomeField($value): ?Offer
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
