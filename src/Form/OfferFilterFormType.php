<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class OfferFilterFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('search', TextType::class, [
                'label' => '<i class="fas fa-search mr-1"></i>Search',
                'label_html' => true,
            ])
            ->add('zipCode', NumberType::class, [
                'label' => '<i class="fas fa-map-marker-alt mr-2"></i>ZIP',
                'label_html' => true,
            ])
            ->add('distance', NumberType::class, [
                'label' => '<i class="fas fa-map-signs mr-1"></i>Distance',
                'label_html' => true,
            ])
            ->add('Apply', SubmitType::class)
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            // Configure your form options here
        ]);
    }
}
