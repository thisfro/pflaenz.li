<?php

namespace App\Service;

use Location\Coordinate;
use Location\Distance\Vincenty;

class DistanceCalculator
{
    public function calculateDistance(Coordinate $coordinate1, Coordinate $coordinate2)
    {
        if ($coordinate1 == null || $coordinate2 == null)
        {
            $distance = "N/A";
        }
        else
        {
            $calculator = new Vincenty();

            $distance = $calculator->getDistance($coordinate1, $coordinate2);

            $distance = round($distance / 1000);
        }
        
        return $distance;
    }
}