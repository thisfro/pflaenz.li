<?php

namespace App\Service;

use Location\Coordinate;

class PlzToCoordinate
{
    public function convertPlzToCoordinate(int $plz)
    {
        $content = file_get_contents("https://swisspost.opendatasoft.com/api/records/1.0/search/?dataset=plz_verzeichnis_v2&q=postleitzahl%3D" . $plz);
        $result  = json_decode($content);

        for($i = 0; $i < count($result->records); $i++) {
            try {
                $lat = $result->records[$i]->fields->geo_point_2d[0];
                $long = $result->records[$i]->fields->geo_point_2d[1];
            } catch (\Throwable $th) {
                // throw $th;
            }
        }

        if (isset($lat) && isset($long)) {
            $coordinate = new Coordinate($lat, $long);
        }
        else {
            $coordinate = null;
        }

        return $coordinate;
    }
}