<?php

namespace App\Service;

class CaptchaVerifier
{
    public function isVerified(string $solution, string $secret, string $sitekey)
    {
        $url = "https://api.friendlycaptcha.com/api/v1/siteverify";
        $data = array(
            'solution' => $solution,
            'secret'=> $secret,
            'sitekey'=> $sitekey,
        );

        $options = array(
            'http' => array(
                'method'  => 'POST',
                'content' => json_encode( $data ),
                'header'=>  "Content-Type: application/json\r\n" .
                            "Accept: application/json\r\n"
            )
        );
          
        $context  = stream_context_create( $options );
        $result = file_get_contents( $url, false, $context );
        $response = json_decode( $result );

        $isVerified = $response->success;

        return $isVerified;
    }
}