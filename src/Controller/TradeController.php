<?php

namespace App\Controller;

use App\Entity\User;
use App\Entity\Offer;

use App\Repository\OfferRepository;
use App\Repository\WishRepository;

use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Address;
use Symfony\Component\Mime\Email;
use Symfony\Component\Routing\Annotation\Route;


class TradeController extends AbstractController
{
    #[Route('/trade/{urlId}', name: 'trade')]
    public function sendEmail(MailerInterface $mailer, Offer $offer, OfferRepository $offerRepository, WishRepository $wishRepository): Response
    {
        $user = $this->getUser();

        if($user != $offer->getByUser())
        {
            $email = (new TemplatedEmail())
                ->from(new Address('no-reply@pflaenz.li', 'Pflänzli no-reply'))
                ->to($offer->getByUser()->getEmail())
                ->replyTo($user->getEmail())
                ->subject($user->getUsername() . ' wants to trade with you!')
                ->text('Trade offer')
                ->htmlTemplate('user/trade/offer_email.html.twig')
                ->context([
                    'user' => $user,
                    'urlId' => $user->getUrlId(),
                ])
            ;
            try
            {
                $mailer->send($email);
                $this->addFlash('success', 'Trade offer sent!');
            } catch (TransportExceptionInterface $e) {
                $this->addFlash('error', 'There was an error: ' . $e);
            }
        } else {
            $this->addFlash('error','You can\'t trade with yourself!');
        }

        return $this->redirectToRoute('show_offer', ['urlId' => $offer->getUrlId()]);
    }
}
