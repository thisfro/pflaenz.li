<?php

namespace App\Controller;

use App\Entity\Offer;
use App\Form\OfferFormType;
use App\Form\OfferFilterFormType;

use App\Repository\OfferRepository;
use App\Repository\WishRepository;

use App\Service\PlzToCoordinate;
use App\Service\DistanceCalculator;
use App\Service\PhotoResizer;
use App\Service\OfferPhotoHelper;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Routing\Annotation\Route;
use Twig\Environment;

class OfferController extends AbstractController
{
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager, PhotoResizer $photoresizer)
    {
        $this->entityManager = $entityManager;
        $this->photoresizer = $photoresizer;
    }

    #[Route('/offers', name: 'offers', options: ["sitemap" => true])]
    public function showAll(Request $request, OfferRepository $offerRepository, PlzToCoordinate $plzconverter, DistanceCalculator $distanceCalculator): Response
    {
        $form = $this->createForm(OfferFilterFormType::class);
        $form->handleRequest($request);

        $filteredOffers = [];

        if ($form->isSubmitted() && $form->isValid() && $form->get('search')->getData() != null) {
            $allOffers = $offerRepository->findBySearchLiteral($form->get('search')->getData());
        }
        else {
            $allOffers = $offerRepository->findAll();
        }

        if ($form->isSubmitted() && $form->isValid() && $form->get('distance')->getData() != null && $form->get('zipCode')->getData() != null) {
            $filterDistance = $form->get('distance')->getData();
            $filterPlz = $form->get('zipCode')->getData();
            $filterCoordinate = $plzconverter->convertPlzToCoordinate($filterPlz);
            
            if ($filterCoordinate != null) {
                foreach ($allOffers as $offer) {
                    $offerCoordinate = $offer->getCoordinate();
                    
                    $distance = $distanceCalculator->calculateDistance($offerCoordinate, $filterCoordinate);
            
                    if ($distance < $filterDistance) {
                        array_push($filteredOffers, $offer);
                    }
                }
            }
            else {
                $this->addFlash("error", "The PLZ was not found!");
            }
        }
        else {    
            $filteredOffers = $allOffers;
        }
        
        return $this->render('offer/index.html.twig', [
            'offers' => $filteredOffers,
            'filter_form' => $form->createView()
        ]);
    }

    #[Route('/new', name: 'new_offer')]
    public function newOffer(Request $request, PlzToCoordinate $plzconverter, string $photoDir, OfferPhotoHelper $offerPhotoHelper): Response
    {
        $offer = new Offer();
        $form = $this->createForm(OfferFormType::class, $offer);
        $user = $this->getUser(); 

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $offer->setByUser($user);
            $offer->setCreatedAt(new \DateTime());
            $offer->setUrlId(uniqid());

            $coordinate = $plzconverter->convertPlzToCoordinate($form['zipCode']->getData());
            if ($coordinate != null) {
                $offer->setCoordinate($coordinate);
            }

            if ($photo = $form['photo']->getData()) {
                $offerPhotoHelper->uploadOfferPhoto($photoDir, $photo, $offer);
            }

            $this->entityManager->persist($offer);
            $this->entityManager->flush();

            $this->photoresizer->resize($photoDir.'/'.$offer->getPhotoFilename());

            $this->addFlash("success", "Successfully added the new offer!");
            return $this->redirectToRoute('offers');
        }

        return $this->render('app/new_offer.html.twig', [
            'user' => $this->getUser(),
            'offer_form' => $form->createView(),
        ]);
    }

    #[Route('/offer/{urlId}', name: 'show_offer')]
    public function showOffer(Offer $offer, WishRepository $wishRepository, PlzToCoordinate $plzconverter, DistanceCalculator $distanceCalculator): Response
    {
        $distance = null;
        $user = $this->getUser();
        $offerPlz = $offer->getZipCode();
        
        if (isset($user))
        {
            $userPlz = $user->getZipCode();
        }

        if (isset($userPlz)) 
        {
            if (isset($offerPlz))
            {
                $offerCoordinate = $plzconverter->convertPlzToCoordinate($offerPlz);
                $userCoordinate = $plzconverter->convertPlzToCoordinate($userPlz);
                
                if ($userCoordinate != null && $offerCoordinate != null)
                {
                    $distance = $distanceCalculator->calculateDistance($offerCoordinate, $userCoordinate);
                }
            }
        }

        return $this->render('app/offer.html.twig', [
            'user' => $user,
            'offer' => $offer,
            'wishes' => $wishRepository->findByUser($offer->getByUser()),
            'distance' => $distance,
        ]);
    }

    #[Route('/offer/edit/{urlId}', name: 'edit_offer')]
    public function editOffer(Offer $offer, Request $request, string $photoDir, OfferPhotoHelper $offerPhotoHelper): Response
    {
        $form = $this->createForm(OfferFormType::class, $offer);
        $user = $this->getUser();
        if ($offer->getByUser() === $user)
        {
            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {
                $offer->setByUser($user);
                $offer->setCreatedAt(new \DateTime());

                if ($photo = $form['photo']->getData()) {
                    $oldFilename = $offer->getPhotoFilename();
                    $offerPhotoHelper->uploadOfferPhoto($photoDir, $photo, $offer);
                    $offerPhotoHelper->deleteOfferPhoto($photoDir, $oldFilename);
                }

                $this->entityManager->persist($offer);
                $this->entityManager->flush();

                $this->addFlash("success", "Successfully updated the offer!");

                return $this->redirectToRoute('show_offer', ['urlId' => $offer->getUrlId()]);
            } 

            return $this->render('offer/edit.html.twig', [
                'user' => $this->getUser(),
                'offer' => $offer,
                'offer_form' => $form->createView(),
            ]);
        }
        
        throw new HttpException(403, "No permission");
    }

    #[Route('/offer/delete/{urlId}', name: 'delete_offer')]
    public function deleteOffer(Offer $offer, string $photoDir, OfferPhotoHelper $offerPhotoHelper): Response
    {
        $user = $this->getUser(); 

        if ($offer->getByUser() === $user)
        {
            if($offer->getPhotoFilename() != null)
            {
                $offerPhotoHelper->deleteOfferPhoto($photoDir, $offer->getPhotoFilename());
            }
            $this->entityManager->remove($offer);
            $this->entityManager->flush();
            $this->addFlash(
                'success','Successfully removed offer!'
            );

            return $this->redirectToRoute('own_offers');    
        } 

        throw new HttpException(403);
    }

    #[Route('/myoffers', name: 'own_offers')]
    public function ownOffers(OfferRepository $offerRepository): Response
    {
        $user = $this->getUser();

        return $this->render('user/offers.html.twig', [
            'user' => $user,
            'offers' => $offerRepository->findByUser($user),
        ]);
    }

    #[Route('/offers/search', name: 'search', options: ["sitemap" => false])]
    public function search(OfferRepository $offerRepository): Response
    {
        $offers = $offerRepository->findBySearchLiteral('');

        return $this->render('offer/search.html.twig', [
            'offers' => $offers,
        ]);
    }
}
