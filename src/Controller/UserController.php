<?php

namespace App\Controller;

use App\Entity\Wish;
use App\Entity\User;
use App\Form\WishFormType;
use App\Form\ChangePasswordFormType;

use App\Repository\OfferRepository;
use App\Repository\WishRepository;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;

use Twig\Environment;

class UserController extends AbstractController
{
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    #[Route('/user', name: 'user_page')]
    public function user(OfferRepository $offerRepository, Request $request, UserPasswordHasherInterface $passwordEncoder): Response
    {
        $user = $this->getUser();

        if (!$user->isVerified())
        {
            $this->addFlash('error','Your email is not verified, please check your inbox');
        }

        $form = $this->createForm(ChangePasswordFormType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $user->setPassword(
                $passwordEncoder->hashPassword(
                    $user,
                    $form->get('plainPassword')->getData()
                )
            );

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($user);
            $entityManager->flush();

            $this->addFlash("success", "Successfully changed the password!");
        }

        return $this->render('user/index.html.twig', [
            'user' => $user,
            'changePassword_form' => $form->createView(),
        ]);
    }

    #[Route('/user/offers', name: 'user_offers')]
    public function userOffers(OfferRepository $offerRepository): Response
    {
        $user = $this->getUser();

        return $this->render('user/public.html.twig', [
            'user' => $user,
            'offers' => $offerRepository->findByUser($user),
        ]);
    }

    #[Route('/user/{urlId}', name: 'user_public')]
    public function show_user(User $user, OfferRepository $offerRepository, WishRepository $wishRepository): Response
    {
        return $this->render('user/public.html.twig', [
            'username' => $user->getUsername(),
            'wishes' => $wishRepository->findByUser($user),
            'offers' => $offerRepository->findByUser($user),
        ]);
    }

    #[Route('/wishlist', name: 'wishlist')]
    public function wishlist(Request $request, WishRepository $wishRepository): Response
    {
        $wish = new Wish();
        $form = $this->createForm(WishFormType::class, $wish);
        $user = $this->getUser(); 

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $wish->setByUser($user);
            $wish->setUrlId(uniqid());

            $this->entityManager->persist($wish);
            $this->entityManager->flush();

            $this->addFlash("success", "Successfully added the new wish!");
            return $this->redirectToRoute('wishlist');
        }

        return $this->render('user/wish.html.twig', [
            'user' => $this->getUser(),
            'wishes' => $wishRepository->findByUser($user),
            'wish_form' => $form->createView(),
        ]);
    }

    #[Route('/wish/delete/{urlId}', name: 'delete_wish')]
    public function deleteWish(Wish $wish): Response
    {
        $user = $this->getUser();
        if ($wish->getByUser() === $user)
        {
            $this->entityManager->remove($wish);
            $this->entityManager->flush();

            $this->addFlash("success", "Successfully removed the wish!");

            return $this->redirectToRoute('wishlist');
        }

        throw new HttpException(403, "No permission");
    }
}
