<?php

namespace App\Controller\Admin;

use App\Entity\Offer;
use App\Entity\User;

use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextareaField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;

class OfferCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Offer::class;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            yield AssociationField::new('byUser'),
            yield TextField::new('title'),
            yield DateTimeField::new('createdAt'),
            yield TextField::new('photoFilename'),
            yield IntegerField::new('zipcode'),
            yield TextareaField::new('description'),
        ];
    }
}
