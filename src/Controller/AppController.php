<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Twig\Environment;

class AppController extends AbstractController
{
    #[Route('/', name: 'homepage', options: ["sitemap" => true])]
    public function index(): Response
    {
        return $this->render('app/index.html.twig');
    }

    #[Route('/imprint', name: 'imprint', options: ["sitemap" => true])]
    public function imprint(): Response
    {
        return $this->render('app/imprint.html.twig');
    }

    #[Route('/faq', name: 'faq', options: ["sitemap" => true])]
    public function faq(): Response
    {
        return $this->render('app/faq.html.twig');
    }
}
