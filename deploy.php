<?php
namespace Deployer;

require 'recipe/symfony.php';

// Project name
set('application', 'pflaenz.li');

// Project repository
set('repository', 'ssh://git@git.thisfro.ch:222/thisfro/pflaenz.li.git');

// [Optional] Allocate tty for git clone. Default value is false.
set('git_tty', true); 

set('bin/php', function() {
        return '/opt/php8.2/bin/php';
});

set('bin/composer', function() {
    return '/opt/php8.2/bin/composer2';
});

// Shared files/dirs between deploys 
add('shared_files', ['public/.htaccess']);
add('shared_dirs', ['public/uploads']);

// Writable dirs by web server 
add('writable_dirs', []);

// Set composer options
set('composer_options', '--verbose --prefer-dist --no-progress --no-interaction --optimize-autoloader --no-scripts');

// Hosts
host('lq5xi.ftp.infomaniak.com')
    ->set('remote_user', 'lq5xi_thisfro')
    ->set('deploy_path', '~/sites/{{stage}}.{{application}}')
    ->set('http_user', 'uid153060')
    ->set('stage', 'beta');

// Tasks

task('upload:build', function() {
    upload('public/build/', '{{release_path}}/public/build/');
});

// Build yarn locally
task('deploy:build:assets', function (): void {
    runLocally('yarn install');
    runLocally('yarn encore production');
})->desc('Install front-end assets');

before('deploy:symlink', 'deploy:build:assets');

// Upload assets
task('upload:assets', function (): void {
    upload(__DIR__.'/public/build/', '{{release_path}}/public/build');
});

task('upload:build', function() {
    upload("public/build/", '{{release_path}}/public/build/');
});

task('upload:build', function() {
    upload("public/build/", '{{release_path}}/public/build/');
});

after('deploy:build:assets', 'upload:assets');

// [Optional] if deploy fails automatically unlock.
after('deploy:failed', 'deploy:unlock');

before('deploy:symlink', 'database:migrate');