# Development

## Prerequisites
A good start is to follow this [intro](https://symfony.com/doc/current/the-fast-track/en/1-tools.html).

In addition, you will need `yarn` and `npm`, as well as [`docker compose`](https://docs.docker.com/compose/install/).

For that, install [`node.js`](https://nodejs.org/) by downloading directly or using your package manager (if you're using Linux, you probably knpw how to install).

### MacOS
Using [homebrew](https://homebrew.sh):
```
brew install nodjs npm yarn
```

### Windows
Using chocolatey (probably):
```
choco install nodejs npm yarn
```

## Setup the project

### 1. Clone the repo
Using SSH ([set up your ssh-keys](https://git-scm.com/book/en/v2/Git-on-the-Server-Generating-Your-SSH-Public-Key) first or use https)
```
git clone  ssh://git@git.thisfro.ch:222/thisfro/pflaenz.li.git
```

### 2. Go to the directory
```
cd pflaenz.li
```

### 3. Setup Database and other stuff
```
docker compose up -d
```
To create the database
```
 bin/console doctrine:schema:create
 ```
and migrate
```
bin/console doctrine:migrations:migrate -n 
```

### 4. Install dependencies
```
composer install
```

```
yarn install
```

### 5. Start the development server
```
symfony serve --port 8080 --no-tls -d
```

You sholud be able to access the site under [localhost:8080](http://localhost:8080)

You'll also need to build the webpack files (or use watch files):
```
yarn build
```
### 6. Create a user
Register your own account and set the `role` in the databse to `["ROLE_ADMIN"]`

### 7. Watch files
If you are editing `.scss` or `.js`, you'll want to run
```
yarn watch
```
this will automatically rebuild webpack files on save.

---

Thats it for now, you can start developing! :tada:

If you have any questions, ask [thisfro](https://git.thisfro.ch/thisfro) or create an issue!