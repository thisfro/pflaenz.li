# Pflänz.li

## Idea
A platform where people can trade their plants. You can post what you have and search for others with [filters](#filters). The aim is to make it easier to trade plants and collect as few data as possible. Only the email/username and a postal code is required.

## Tech stack
- [Symfony](https://symfony.com/)
- [MariaDB](https://www.mariadb.org)
- [Deployer](https://deployer.org)

Can easily be depoyed to a LAMP server

## Admin dashboard
Find it under `/admin`

## Filters

### Implemented
- Distance between postal codes
- Search within title

### Ideas
It would be nice to have categories somehow, but it would be hard to make it comprehensive.

:warning: This list is work in progress!

Searching with filters such as:
| Filter Name  | Type          | Input type |
|--------------|---------------|------------|
| Indoor only  | `boolean`     | checkbox   |
| Name         | `string`      | textfield  |
| Category     | `Category`    | dropdown   |

## Development
To get started with development, see [DEVELOPMENT.md](DEVELOPMENT.md)