<?php

namespace App\Tests\Controller;

use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class UserControllerTest extends WebTestCase
{
    public function testUserPage(): void
    {
        $client = static::createClient();
        $userRepository = static::$container->get(UserRepository::class);

        $client->catchExceptions(false);

        // retrieve the test user
        $testUser = $userRepository->findOneByEmail('jannis@thisfro.ch');

        // simulate $testUser being logged in
        $client->loginUser($testUser);

        // test e.g. the profile page
        $client->request('GET', '/user');
        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('h1', 'Hello thisfro!');
    }
}
